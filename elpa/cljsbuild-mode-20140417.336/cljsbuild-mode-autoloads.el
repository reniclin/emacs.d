;;; cljsbuild-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (cljsbuild-start cljsbuild-mode) "cljsbuild-mode"
;;;;;;  "cljsbuild-mode.el" (21358 23677 0 0))
;;; Generated autoloads from cljsbuild-mode.el

(autoload 'cljsbuild-mode "cljsbuild-mode" "\
ClojureScript Build mode

\(fn &optional ARG)" t nil)

(autoload 'cljsbuild-start "cljsbuild-mode" "\
Run cljsbuild in a background buffer.

\(fn CMD)" t nil)

;;;***

;;;### (autoloads nil nil ("cljsbuild-mode-pkg.el") (21358 23677
;;;;;;  905166 0))

;;;***

(provide 'cljsbuild-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cljsbuild-mode-autoloads.el ends here

;; (byte-recompile-directory (expand-file-name "~/.emacs.d") 0)
;; ===== add for use package, eq. M-x package-install =====
(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
;; (add-to-list 'package-archives
;;             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(package-initialize)
;;
;;
;; ===== Blink Cursor =====
(blink-cursor-mode 1)
;;
;;
;; ===== open line number =====
(global-linum-mode 1)
;;
;;
;; ===== 把游標設成一條槓 =====
(setq-default cursor-type 'bar)
;;
;;
;; ===== display time =====
(display-time)
;;
;;
;; ===== add my custom el file path =====
(add-to-list 'load-path (expand-file-name "~/.emacs.d/my-custom"))
;; ===== my custom el setting =====
;;(require 'my-color-theme)
(require 'my-font-set)
(require 'my-coffee-custom)
;; ===== add for 80 column highlight =====
;; (require 'highlight-80+)
;;
;;
;; ===== taglist-mode
(require 'taglist)
(global-set-key (kbd "C-x t" ) 'taglist)
;;
;;
;; ===== fci mode =====
(require 'fill-column-indicator)
(add-hook 'c++-mode-hook 'fci-mode)
(setq-default fill-column 80)
;;
;;
;; ===== RainbowDelimiters =====
(require 'rainbow-delimiters)
;; - To use only with specific modes, add lines like the following:
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'clojurescript-mode-hook 'rainbow-delimiters-mode)
;; - To enable in all programming-related modes (Emacs 24+):
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
;; - To use Emacs-wide, add this line:
;; (global-rainbow-delimiters-mode)
;;
;;
;; ===== 縮排設定 =====
;;(setq c-default-style "linux")
;; (setq-default c-basic-offset 4
;;               tab-width 4
;;               indent-tabs-mode nil)
;;
;; ===== google c style =====
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)
;;
;;
;; ===== add for auto-complete =====
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elpa/auto-complete-20140414.2324/dict")
(ac-config-default)
(add-hook 'c++-mode-hook 'auto-complete-mode)
(add-hook 'qml-mode-hook 'auto-complete-mode)
(add-hook 'python-mode-hook 'auto-complete-mode)
(add-hook 'js2-mode-hook 'auto-complete-mode)
(add-hook 'clojure-mode-hook 'auto-complete-mode)
(add-hook 'clojurescript-mode-hook 'auto-complete-mode)
;;
;;
;; ===== open tabbar-mode =====
;;(tabbar-mode 1)
;;
;; ===== enable tabbar-ruler =====
(setq EmacsPortable-global-tabbar 't) ;; 開啟標籤欄支持
;; (setq EmacsPortable-global-ruler 't)  ;; 開啟全局標尺
(setq EmacsPortable-popup-menu 't)    ;; 彈出式菜單
;; (setq EmacsPortable-popup-toolbar 't) ;; 彈出式工具欄
(require 'tabbar-ruler)
;;
;;
;;
;; ===== add for use color theme =====
(add-to-list 'load-path "~/.emacs.d/color-theme")
(require 'color-theme)
(eval-after-load "color-theme"
 '(progn
    (color-theme-initialize)
    (color-theme-tangotango)))
;;
;; ===== to set foreground color to white =====
;;(set-foreground-color "LightSteelBlue1")
;; (set-foreground-color "grey13")
;; (set-foreground-color "grey98")
;;
;; ===== to set background color to black =====
;; (set-background-color "grey95")
;; (set-background-color "white")
;; (set-background-color "grey11")
;; (set-background-color "grey20")
;;
;; ===== set region color =====
;; (set-face-background 'region "CornflowerBlue")
(set-face-background 'region "RoyalBlue4")
;;
;;
;; ===== set comment color =====
;; (set-face-foreground 'font-lock-comment-face "DarkSeaGreen" )
;; (set-face-foreground 'font-lock-comment-face "MediumSeaGreen" )
;; (set-face-foreground 'font-lock-comment-face "Green4" )
;; (set-face-foreground 'font-lock-comment-face "LightYellow4" )
;; (set-face-foreground 'font-lock-comment-face "thistle3" )
;; (set-face-foreground 'font-lock-comment-face "MediumAquamarine" )
;; (set-face-foreground 'font-lock-comment-face "SlateGray4" )
(set-face-foreground 'font-lock-comment-face "grey60" )
;;
;;
;; ===== highlight search =====
(setq search-highlight t)
;;
;;; ===== Shell mode =====
;; (setq ansi-color-names-vector ; better contrast colors
;;   ["gray40" "VioletRed1" "green1" "yellow1"
;;    "SkyBlue1" "orchid1" "CadetBlue1" "gray80"])
(setq ansi-color-names-vector ; better contrast colors
  ["gray60" "VioletRed1" "green1" "yellow1"
   "SkyBlue1" "orchid1" "CadetBlue1" "white"])
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
;;
;;
;;
;; ===== add for emacs ruby versions =====
;; Load rvm.el
(require 'rvm)
;; use rvm’s default ruby for the current Emacs session
(rvm-use-default)
;;
;;
;;
;; ===== add for use qml mode =====
(add-to-list 'load-path "~/.emacs.d/elpa/qml-mode-0.3/")
(require 'qml-mode)
;;
;;
;;
;; ===== add for use js2 mode =====
(require 'js2-mode)
;;
;;
;;
;; ====== add for project-mode
;;(add-to-list 'load-path "~/.emacs.d/elpa/project-mode-1.0/")
;;(require 'project-mode)
;;
;;
;; ===== add for CEDET ====
;;(require 'cedet)
;;(require 'eieio)
;;(require 'semantic)
;;(require 'srecode)
;;(require 'ede)
;;(require 'speedbar)
:;
;;
;; ===== add for cscope =====
;; (require 'xcscope)
;;
;;
;; ===== add require for ecb =====
;; (add-to-list 'load-path "~/.emacs.d/elpa/ecb-2.40/")
;; (setq stack-trace-on-error t)
;; (require 'ecb)
;; (require 'ecb-autoloads)
;;
;;
;; ===== for ecb easy use key mapping
;;(global-set-key (kbd "<M-left>") 'ecb-goto-window-methods)
;;(global-set-key (kbd "<M-right>") 'ecb-goto-window-edit1)
;;(global-set-key (kbd "C-c C-e") 'ecb-activate)
;;(global-set-key (kbd "C-c C-x C-e" ) 'ecb-deactivate)
;;
;;
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.40"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;; '(hl-line ((t (:background "grey87"))))
 '(hl-line ((t (:background "grey23")))))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

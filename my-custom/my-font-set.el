(provide 'my-font-set)
;; 字體設置, 你的系統中很可能沒有 Monaco, 那麼你就選擇一個你喜歡的字體, 代替它寫在下面.
(set-default-font "Monaco-13")
;; 設置視窗模式下的中文字體, 不會影響到 terminal 模式 (-nw)
(if (display-graphic-p)
    (set-fontset-font (frame-parameter nil 'font)
                      'han
                      (font-spec :family "Heiti TC")
                      ;; (font-spec :family "LiHei Pro")
                      ))


;; (create-fontset-from-fontset-spec
;;  "-apple-monaco-medium-r-normal--14-*-*-*-*-*-fontset-monaco,
;;     ascii:-apple-monaco-medium-r-normal--14-140-75-75-m-140-mac-roman,
;;     latin-iso8859-1:-apple-monaco-medium-r-normal--14-140-75-75-m-140-mac-roman")
;; (set-frame-font "fontset-monaco")


;; ;; 建立新的 fontset
;; (create-fontset-from-fontset-spec
;;     "-apple-monaco-medium-r-normal--14-*-*-*-*-*-fontset-coldnew")
;; ;; 設定其他編碼的字型
;; (set-fontset-font "fontset-coldnew"     ; 中文字體
;;     'han (font-spec :family "LiHei Pro" :size 13))
;; (set-fontset-font "fontset-coldnew"     ; 符號
;;     'symbol (font-spec :family "Monaco" :size 13 ))
;; ;; 使用自己建立的 fontset
;; (set-frame-font "fontset-coldnew")
